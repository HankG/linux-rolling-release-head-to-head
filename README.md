# Linux Rolling Release Head to Head

Raw data and statistics for my Linux Rolling Release Head to Head study. 
This is attempting to study the performance and overhead associated with 
various Linux rolling release distributions.

Any custom source code generated for running or processing these results is 
released under an AGPLv3.0 license. All data is being released under a 
Creative Commons Attribution (CC-BY) license.
