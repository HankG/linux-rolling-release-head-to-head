[root@archrollingtest user1]# time sudo pacman -Syu --noconfirm
:: Synchronizing package databases...
 core                  133.5 KiB   172 KiB/s 00:01 [######################] 100%
 extra                1566.2 KiB  3.55 MiB/s 00:00 [######################] 100%
 community               5.6 MiB  6.15 MiB/s 00:01 [######################] 100%
:: Starting full system upgrade...
resolving dependencies...
looking for conflicting packages...

Packages (24) appstream-0.14.4-1  blas-3.9.1-2  evolution-data-server-3.40.2-2
              firefox-89.0.2-1  gd-2.3.2-4  gdbm-1.20-1  groff-1.22.4-6
              iana-etc-20210624-1  imagemagick-7.1.0.2-1  lapack-3.9.1-2
              libavif-0.9.2-1  libcanberra-0.30+2+gc0620e4-5  libldap-2.4.59-1
              libphonenumber-8.12.25-1  libxcrypt-4.4.23-1
              linux-5.12.13.arch1-2  mesa-21.1.3-1  protobuf-3.17.3-1
              sqlite-3.36.0-1  totem-pl-parser-3.26.6-1
              virtualbox-guest-utils-6.1.22-3  vulkan-headers-1:1.2.182-1
              vulkan-icd-loader-1.2.182-1  xf86-input-libinput-1.1.0-1

Total Download Size:   195.57 MiB
Total Installed Size:  560.91 MiB
Net Upgrade Size:        0.96 MiB

:: Proceed with installation? [Y/n] 
:: Retrieving packages...
 iana-etc-2021062...   391.1 KiB  1160 KiB/s 00:00 [######################] 100%
 libldap-2.4.59-1...   303.5 KiB  1862 KiB/s 00:00 [######################] 100%
 libxcrypt-4.4.23...    88.5 KiB  1017 KiB/s 00:00 [######################] 100%
 sqlite-3.36.0-1-...  1741.8 KiB  2.38 MiB/s 00:01 [######################] 100%
 appstream-0.14.4...     2.0 MiB  1948 KiB/s 00:01 [######################] 100%
 blas-3.9.1-2-x86_64   124.5 KiB  1431 KiB/s 00:00 [######################] 100%
 protobuf-3.17.3-...     2.1 MiB  1885 KiB/s 00:01 [######################] 100%
 libphonenumber-8...  2010.6 KiB  2043 KiB/s 00:01 [######################] 100%
 libcanberra-0.30...    91.6 KiB  1018 KiB/s 00:00 [######################] 100%
 vulkan-icd-loade...   109.6 KiB  1371 KiB/s 00:00 [######################] 100%
 mesa-21.1.3-1-x86_64   17.5 MiB  2.47 MiB/s 00:07 [######################] 100%
 gdbm-1.20-1-x86_64    220.8 KiB  1355 KiB/s 00:00 [######################] 100%
 evolution-data-s...     4.5 MiB  2.67 MiB/s 00:02 [######################] 100%
 firefox-89.0.2-1...    60.1 MiB  3.74 MiB/s 00:16 [######################] 100%
 libavif-0.9.2-1-...   103.9 KiB  1208 KiB/s 00:00 [######################] 100%
 gd-2.3.2-4-x86_64     160.7 KiB  1868 KiB/s 00:00 [######################] 100%
 groff-1.22.4-6-x...     2.1 MiB  2.65 MiB/s 00:01 [######################] 100%
 imagemagick-7.1....     2.5 MiB  2.58 MiB/s 00:01 [######################] 100%
 lapack-3.9.1-2-x...     2.3 MiB  2.07 MiB/s 00:01 [######################] 100%
 linux-5.12.13.ar...    95.1 MiB  3.72 MiB/s 00:26 [######################] 100%
 totem-pl-parser-...   125.3 KiB  1457 KiB/s 00:00 [######################] 100%
 virtualbox-guest...  1261.6 KiB  4.34 MiB/s 00:00 [######################] 100%
 vulkan-headers-1...   723.2 KiB  2.79 MiB/s 00:00 [######################] 100%
 xf86-input-libin...    37.3 KiB   450 KiB/s 00:00 [######################] 100%
 Total (24/24)         195.6 MiB  3.21 MiB/s 01:01 [######################] 100%
(24/24) checking keys in keyring                   [######################] 100%
(24/24) checking package integrity                 [######################] 100%
(24/24) loading package files                      [######################] 100%
(24/24) checking for file conflicts                [######################] 100%
(24/24) checking available disk space              [######################] 100%
:: Running pre-transaction hooks...
(1/1) Removing linux initcpios...
:: Processing package changes...
( 1/24) upgrading iana-etc                         [######################] 100%
( 2/24) upgrading libldap                          [######################] 100%
( 3/24) upgrading libxcrypt                        [######################] 100%
( 4/24) upgrading sqlite                           [######################] 100%
( 5/24) upgrading appstream                        [######################] 100%
( 6/24) upgrading blas                             [######################] 100%
( 7/24) upgrading protobuf                         [######################] 100%
( 8/24) upgrading libphonenumber                   [######################] 100%
( 9/24) upgrading libcanberra                      [######################] 100%
(10/24) upgrading vulkan-icd-loader                [######################] 100%
(11/24) upgrading mesa                             [######################] 100%
(12/24) upgrading gdbm                             [######################] 100%
(13/24) upgrading evolution-data-server            [######################] 100%
(14/24) upgrading firefox                          [######################] 100%
(15/24) upgrading libavif                          [######################] 100%
(16/24) upgrading gd                               [######################] 100%
(17/24) upgrading groff                            [######################] 100%
(18/24) upgrading imagemagick                      [######################] 100%
(19/24) upgrading lapack                           [######################] 100%
(20/24) upgrading linux                            [######################] 100%
(21/24) upgrading totem-pl-parser                  [######################] 100%
(22/24) upgrading virtualbox-guest-utils           [######################] 100%
(23/24) upgrading vulkan-headers                   [######################] 100%
(24/24) upgrading xf86-input-libinput              [######################] 100%
:: Running post-transaction hooks...
( 1/13) Creating system user accounts...
( 2/13) Reloading system manager configuration...
( 3/13) Reloading device manager configuration...
( 4/13) Arming ConditionNeedsUpdate...
( 5/13) Updating module dependencies...
( 6/13) Updating linux initcpios...
==> Building image from preset: /etc/mkinitcpio.d/linux.preset: 'default'
  -> -k /boot/vmlinuz-linux -c /etc/mkinitcpio.conf -g /boot/initramfs-linux.img
==> Starting build: 5.12.13-arch1-2
  -> Running build hook: [base]
  -> Running build hook: [udev]
  -> Running build hook: [autodetect]
  -> Running build hook: [keyboard]
  -> Running build hook: [keymap]
  -> Running build hook: [modconf]
  -> Running build hook: [block]
  -> Running build hook: [filesystems]
  -> Running build hook: [fsck]
==> Generating module dependencies
==> Creating zstd-compressed initcpio image: /boot/initramfs-linux.img
==> Image generation successful
==> Building image from preset: /etc/mkinitcpio.d/linux.preset: 'fallback'
  -> -k /boot/vmlinuz-linux -c /etc/mkinitcpio.conf -g /boot/initramfs-linux-fallback.img -S autodetect
==> Starting build: 5.12.13-arch1-2
  -> Running build hook: [base]
  -> Running build hook: [udev]
  -> Running build hook: [keyboard]
==> WARNING: Possibly missing firmware for module: xhci_pci
  -> Running build hook: [keymap]
  -> Running build hook: [modconf]
  -> Running build hook: [block]
==> WARNING: Possibly missing firmware for module: wd719x
==> WARNING: Possibly missing firmware for module: aic94xx
  -> Running build hook: [filesystems]
  -> Running build hook: [fsck]
==> Generating module dependencies
==> Creating zstd-compressed initcpio image: /boot/initramfs-linux-fallback.img
==> Image generation successful
( 7/13) Warn about old perl modules
( 8/13) Probing GDK-Pixbuf loader modules...
( 9/13) Compiling GSettings XML schema files...
(10/13) Updating icon theme caches...
(11/13) Updating the info directory file...
(12/13) Updating the appstream cache...
AppStream cache update completed successfully.
(13/13) Updating the desktop file MIME type cache...

real	1m31.431s
user	0m19.743s
sys	0m8.271s
[root@archrollingtest user1]# time sudo pacman -Sc --noconfirm
Packages to keep:
  All locally installed packages

Cache directory: /var/cache/pacman/pkg/
:: Do you want to remove all other packages from cache? [Y/n] 
removing old packages from cache...

Database directory: /var/lib/pacman/
:: Do you want to remove unused repositories? [Y/n] 
removing unused sync repositories...

real	0m1.136s
user	0m0.293s
sys	0m0.199s
[root@archrollingtest user1]# time sudo paccache -r 
sudo: paccache: command not found

real	0m0.025s
user	0m0.014s
sys	0m0.010s
[root@archrollingtest user1]# 
