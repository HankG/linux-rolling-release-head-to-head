Filesystem     1K-blocks      Used Available Use% Mounted on
devtmpfs         1992536         8   1992528   1% /dev
tmpfs            2005056         0   2005056   0% /dev/shm
tmpfs             802024      1528    800496   1% /run
/dev/sda2       39835648  15337248  23556752  40% /
tmpfs            2005060         8   2005052   1% /tmp
/dev/sda2       39835648  15337248  23556752  40% /.snapshots
/dev/sda2       39835648  15337248  23556752  40% /boot/grub2/x86_64-efi
/dev/sda2       39835648  15337248  23556752  40% /home
/dev/sda2       39835648  15337248  23556752  40% /opt
/dev/sda2       39835648  15337248  23556752  40% /srv
/dev/sda2       39835648  15337248  23556752  40% /usr/local
/dev/sda2       39835648  15337248  23556752  40% /root
/dev/sda2       39835648  15337248  23556752  40% /boot/grub2/i386-pc
/dev/sda2       39835648  15337248  23556752  40% /var
VmShared       960384328 639308448 321075880  67% /media/sf_VmShared
tmpfs             401008        92    400916   1% /run/user/1000
/dev/sr0           59558     59558         0 100% /run/media/user1/VBox_GAs_6.1.16
