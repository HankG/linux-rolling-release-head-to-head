Filesystem     1K-blocks      Used Available Use% Mounted on
devtmpfs         1992516         8   1992508   1% /dev
tmpfs            2005056         0   2005056   0% /dev/shm
tmpfs             802024      1524    800500   1% /run
/dev/sda2       39835648  17761196  21295252  46% /
tmpfs            2005060         8   2005052   1% /tmp
/dev/sda2       39835648  17761196  21295252  46% /.snapshots
/dev/sda2       39835648  17761196  21295252  46% /boot/grub2/i386-pc
/dev/sda2       39835648  17761196  21295252  46% /boot/grub2/x86_64-efi
/dev/sda2       39835648  17761196  21295252  46% /home
/dev/sda2       39835648  17761196  21295252  46% /root
/dev/sda2       39835648  17761196  21295252  46% /usr/local
/dev/sda2       39835648  17761196  21295252  46% /opt
/dev/sda2       39835648  17761196  21295252  46% /srv
/dev/sda2       39835648  17761196  21295252  46% /var
VmShared       960384328 683539500 276844828  72% /media/sf_VmShared
tmpfs             401008        84    400924   1% /run/user/1000
/dev/sr0           59558     59558         0 100% /run/media/user1/VBox_GAs_6.1.16
