>>> Extracting dev-python/subunit-1.4.0

>>> Installing (99 of 187) dev-python/subunit-1.4.0::gentoo

>>> Emerging binary (100 of 187) dev-python/fixtures-3.0.0-r1::gentoo
 * fixtures-3.0.0-r1.tbz2 size ;-) ...                                   [ ok ]
>>> Extracting info
>>> Extracting dev-python/fixtures-3.0.0-r1

>>> Installing (100 of 187) dev-python/fixtures-3.0.0-r1::gentoo

>>> Emerging binary (101 of 187) net-nds/openldap-2.4.59-r1::gentoo
 * openldap-2.4.59-r1.tbz2 size ;-) ...                                  [ ok ]
>>> Extracting info
 * Skipping scan for previous datadirs as requested by minimal useflag
>>> Extracting net-nds/openldap-2.4.59-r1

>>> Installing (101 of 187) net-nds/openldap-2.4.59-r1::gentoo

>>> Emerging binary (102 of 187) net-misc/curl-7.77.0-r1::gentoo
 * curl-7.77.0-r1.tbz2 size ;-) ...                                      [ ok ]
>>> Extracting info
>>> Extracting net-misc/curl-7.77.0-r1

>>> Installing (102 of 187) net-misc/curl-7.77.0-r1::gentoo

>>> Emerging binary (103 of 187) dev-util/cmake-3.20.5::gentoo
 * cmake-3.20.5.tbz2 size ;-) ...                                        [ ok ]
>>> Extracting info
>>> Extracting dev-util/cmake-3.20.5

>>> Installing (103 of 187) dev-util/cmake-3.20.5::gentoo

>>> Emerging binary (104 of 187) dev-lang/rust-1.53.0::gentoo
 * rust-1.53.0.tbz2 size ;-) ...                                         [ ok ]
>>> Extracting info
>>> Extracting dev-lang/rust-1.53.0

>>> Installing (104 of 187) dev-lang/rust-1.53.0::gentoo
Marking the latest still installed version as default...
 * Rust installs a helper script for calling GDB and LLDB,
 * for your convenience it is installed under /usr/bin/rust-{gdb,lldb}-1.53.0.
 * install app-vim/rust-vim to get vim support for rust.

>>> Emerging binary (105 of 187) media-libs/mesa-21.1.3::gentoo
 * mesa-21.1.3.tbz2 size ;-) ...                                         [ ok ]
>>> Extracting info
 * Checking for suitable kernel configuration options...
 [ ok ]
>>> Extracting media-libs/mesa-21.1.3

>>> Installing (105 of 187) media-libs/mesa-21.1.3::gentoo

>>> Emerging binary (106 of 187) gui-libs/libwpe-1.10.1::gentoo
 * libwpe-1.10.1.tbz2 size ;-) ...                                       [ ok ]
>>> Extracting info
>>> Extracting gui-libs/libwpe-1.10.1

>>> Installing (106 of 187) gui-libs/libwpe-1.10.1::gentoo

>>> Emerging binary (107 of 187) dev-python/pycairo-1.20.1::gentoo
 * pycairo-1.20.1.tbz2 size ;-) ...                                      [ ok ]
>>> Extracting info
>>> Extracting dev-python/pycairo-1.20.1

>>> Installing (107 of 187) dev-python/pycairo-1.20.1::gentoo

>>> Emerging binary (108 of 187) media-libs/libplacebo-2.72.2::gentoo
 * libplacebo-2.72.2.tbz2 size ;-) ...                                   [ ok ]
>>> Extracting info
>>> Extracting media-libs/libplacebo-2.72.2

>>> Installing (108 of 187) media-libs/libplacebo-2.72.2::gentoo

>>> Emerging binary (109 of 187) gui-libs/wpebackend-fdo-1.10.0::gentoo
 * wpebackend-fdo-1.10.0.tbz2 size ;-) ...                               [ ok ]
>>> Extracting info
>>> Extracting gui-libs/wpebackend-fdo-1.10.0

>>> Installing (109 of 187) gui-libs/wpebackend-fdo-1.10.0::gentoo

>>> Emerging binary (110 of 187) dev-python/pygobject-3.40.1-r1::gentoo
 * pygobject-3.40.1-r1.tbz2 size ;-) ...                                 [ ok ]
>>> Extracting info
>>> Extracting dev-python/pygobject-3.40.1-r1

>>> Installing (110 of 187) dev-python/pygobject-3.40.1-r1::gentoo

>>> Emerging binary (111 of 187) net-libs/libaccounts-glib-1.25-r2::gentoo
 * libaccounts-glib-1.25-r2.tbz2 size ;-) ...                            [ ok ]
>>> Extracting info
>>> Extracting net-libs/libaccounts-glib-1.25-r2

>>> Installing (111 of 187) net-libs/libaccounts-glib-1.25-r2::gentoo

>>> Emerging binary (112 of 187) dev-python/pyopengl-3.1.5::gentoo
 * pyopengl-3.1.5.tbz2 size ;-) ...                                      [ ok ]
>>> Extracting info
>>> Extracting dev-python/pyopengl-3.1.5

>>> Installing (112 of 187) dev-python/pyopengl-3.1.5::gentoo

>>> Emerging binary (113 of 187) media-libs/gexiv2-0.12.2::gentoo
 * gexiv2-0.12.2.tbz2 size ;-) ...                                       [ ok ]
>>> Extracting info
>>> Extracting media-libs/gexiv2-0.12.2

>>> Installing (113 of 187) media-libs/gexiv2-0.12.2::gentoo

>>> Emerging binary (114 of 187) app-emulation/spice-0.15.0::gentoo
 * spice-0.15.0.tbz2 size ;-) ...                                        [ ok ]
>>> Extracting info
>>> Extracting app-emulation/spice-0.15.0

>>> Installing (114 of 187) app-emulation/spice-0.15.0::gentoo

>>> Emerging binary (115 of 187) net-wireless/bluez-5.59-r1::gentoo
 * bluez-5.59-r1.tbz2 size ;-) ...                                       [ ok ]
>>> Extracting info
 * Checking for suitable kernel configuration options...
 [ ok ]
>>> Extracting net-wireless/bluez-5.59-r1

>>> Installing (115 of 187) net-wireless/bluez-5.59-r1::gentoo
 * Running udev control --reload for reloading rules and databases ...
 [ ok ]

>>> Emerging binary (116 of 187) dev-python/pillow-8.2.0::gentoo
 * pillow-8.2.0.tbz2 size ;-) ...                                        [ ok ]
>>> Extracting info
>>> Extracting dev-python/pillow-8.2.0

>>> Installing (116 of 187) dev-python/pillow-8.2.0::gentoo

>>> Emerging binary (117 of 187) kde-plasma/xembed-sni-proxy-5.22.2::gentoo
 * xembed-sni-proxy-5.22.2.tbz2 size ;-) ...                             [ ok ]
>>> Extracting info
>>> Extracting kde-plasma/xembed-sni-proxy-5.22.2

>>> Installing (117 of 187) kde-plasma/xembed-sni-proxy-5.22.2::gentoo

>>> Emerging binary (118 of 187) net-dialup/ppp-2.4.9-r3::gentoo
 * ppp-2.4.9-r3.tbz2 size ;-) ...                                        [ ok ]
>>> Extracting info
>>> Extracting net-dialup/ppp-2.4.9-r3

>>> Installing (118 of 187) net-dialup/ppp-2.4.9-r3::gentoo
 * >>> SetUID: [chmod go-r] /usr/sbin/pppd ...
 [ ok ]

 * Pon, poff and plog scripts have been supplied for experienced users.
 * Users needing particular scripts (ssh,rsh,etc.) should check out the
 * /usr/share/doc/ppp-2.4.9-r3/scripts directory.
 * "rp-pppoe.so" plugin has been renamed to "pppoe.so"

>>> Emerging binary (119 of 187) app-crypt/gpgme-1.15.1::gentoo
 * gpgme-1.15.1.tbz2 size ;-) ...                                        [ ok ]
>>> Extracting info
>>> Extracting app-crypt/gpgme-1.15.1

>>> Installing (119 of 187) app-crypt/gpgme-1.15.1::gentoo

>>> Emerging binary (120 of 187) app-portage/gemato-16.2::gentoo
 * gemato-16.2.tbz2 size ;-) ...                                         [ ok ]
>>> Extracting info
>>> Extracting app-portage/gemato-16.2

>>> Installing (120 of 187) app-portage/gemato-16.2::gentoo

>>> Emerging binary (121 of 187) sys-apps/portage-3.0.20-r6::gentoo
 * portage-3.0.20-r6.tbz2 size ;-) ...                                   [ ok ]
>>> Extracting info
>>> Extracting sys-apps/portage-3.0.20-r6

>>> Installing (121 of 187) sys-apps/portage-3.0.20-r6::gentoo
 * Using python3.9 in global scope
 * Setting make.globals default BINPKG_COMPRESS="bzip2" for backward compatibility
 * Setting make.globals default FEATURES="${FEATURES} -binpkg-multi-instance" for backward compatibility

>>> Emerging binary (122 of 187) dev-libs/libjcat-0.1.8::gentoo
 * libjcat-0.1.8.tbz2 size ;-) ...                                       [ ok ]
>>> Extracting info
>>> Extracting dev-libs/libjcat-0.1.8

>>> Installing (122 of 187) dev-libs/libjcat-0.1.8::gentoo

>>> Emerging binary (123 of 187) dev-python/GitPython-3.1.18::gentoo
 * GitPython-3.1.18.tbz2 size ;-) ...                                    [ ok ]
>>> Extracting info
>>> Extracting dev-python/GitPython-3.1.18

>>> Installing (123 of 187) dev-python/GitPython-3.1.18::gentoo

>>> Emerging binary (124 of 187) dev-java/java-config-2.3.1::gentoo
 * java-config-2.3.1.tbz2 size ;-) ...                                   [ ok ]
>>> Extracting info
>>> Extracting dev-java/java-config-2.3.1

>>> Installing (124 of 187) dev-java/java-config-2.3.1::gentoo

>>> Emerging binary (125 of 187) app-portage/gentoolkit-0.5.1::gentoo
 * gentoolkit-0.5.1.tbz2 size ;-) ...                                    [ ok ]
>>> Extracting info
>>> Extracting app-portage/gentoolkit-0.5.1

>>> Installing (125 of 187) app-portage/gentoolkit-0.5.1::gentoo

>>> Emerging binary (126 of 187) dev-java/openjdk-bin-8.292_p10::gentoo
 * openjdk-bin-8.292_p10.tbz2 size ;-) ...                               [ ok ]
>>> Extracting info
>>> Extracting dev-java/openjdk-bin-8.292_p10

>>> Installing (126 of 187) dev-java/openjdk-bin-8.292_p10::gentoo
 * QA Notice: Symbolic link /opt/openjdk-bin-8.292_p10/jre/lib/amd64/server/libjsig.debuginfo points to /opt/openjdk-bin-8.292_p10/jre/lib/amd64/libjsig.debuginfo which does not exist.
 * openjdk-bin-8.292_p10 set as the default system-vm.
 * Updating .desktop files database ...
 [ ok ]

>>> Emerging binary (127 of 187) dev-java/openjdk-bin-11.0.11_p9-r1::gentoo
 * openjdk-bin-11.0.11_p9-r1.tbz2 size ;-) ...                           [ ok ]
>>> Extracting info
>>> Extracting dev-java/openjdk-bin-11.0.11_p9-r1

>>> Installing (127 of 187) dev-java/openjdk-bin-11.0.11_p9-r1::gentoo
 * Updating .desktop files database ...
 [ ok ]
 * The experimental gentoo-vm USE flag has not been enabled so this JDK
 * will not be recognised by the system. For example, simply calling
 * "java" will launch a different JVM. This is necessary until Gentoo
 * fully supports Java 11. This JDK must therefore be invoked using its
 * absolute location under /opt/openjdk-bin-11.0.11_p9.

>>> Emerging binary (128 of 187) dev-java/openjdk-jre-bin-11.0.11_p9::gentoo
 * openjdk-jre-bin-11.0.11_p9.tbz2 size ;-) ...                          [ ok ]
>>> Extracting info
>>> Extracting dev-java/openjdk-jre-bin-11.0.11_p9

>>> Installing (128 of 187) dev-java/openjdk-jre-bin-11.0.11_p9::gentoo
 * Updating .desktop files database ...
 [ ok ]
 * The experimental gentoo-vm USE flag has not been enabled so this JRE
 * will not be recognised by the system. For example, simply calling
 * "java" will launch a different JVM. This is necessary until Gentoo
 * fully supports Java 11. This JRE must therefore be invoked using its
 * absolute location under /opt/openjdk-jre-bin-11.0.11_p9.

>>> Emerging binary (129 of 187) virtual/jdk-1.8.0-r10::redcore
 * jdk-1.8.0-r10.tbz2 size ;-) ...                                       [ ok ]
>>> Extracting info
>>> Extracting virtual/jdk-1.8.0-r10

>>> Installing (129 of 187) virtual/jdk-1.8.0-r10::redcore

>>> Emerging binary (130 of 187) dev-java/ant-core-1.10.9::gentoo
 * ant-core-1.10.9.tbz2 size ;-) ...                                     [ ok ]
>>> Extracting info
>>> Extracting dev-java/ant-core-1.10.9

>>> Installing (130 of 187) dev-java/ant-core-1.10.9::gentoo

>>> Emerging binary (131 of 187) media-video/mpv-0.33.1-r1::gentoo
 * mpv-0.33.1-r1.tbz2 size ;-) ...                                       [ ok ]
>>> Extracting info
>>> Extracting media-video/mpv-0.33.1-r1

>>> Installing (131 of 187) media-video/mpv-0.33.1-r1::gentoo
 * Updating icons cache ...
 [ ok ]
 * Updating .desktop files database ...
 [ ok ]
 * Install additional packages for optional runtime features:
 *   net-misc/youtube-dl for URL support
 * Updating icons cache ...
 [ ok ]
 * Updating .desktop files database ...
 [ ok ]

>>> Emerging binary (132 of 187) media-video/vlc-3.0.16-r2::gentoo
 * vlc-3.0.16-r2.tbz2 size ;-) ...                                       [ ok ]
>>> Extracting info
>>> Extracting media-video/vlc-3.0.16-r2

>>> Installing (132 of 187) media-video/vlc-3.0.16-r2::gentoo
 * Updating .desktop files database ...
 [ ok ]
 * Updating icons cache ...
 [ ok ]
 * Running /usr/lib64/vlc/vlc-cache-gen on /usr/lib64/vlc/plugins/
 * Updating .desktop files database ...
 [ ok ]
 * Updating icons cache ...
 [ ok ]

>>> Emerging binary (133 of 187) www-client/firefox-89.0.2::gentoo
 * firefox-89.0.2.tbz2 size ;-) ...                                      [ ok ]
>>> Extracting info
>>> Extracting www-client/firefox-89.0.2

>>> Installing (133 of 187) www-client/firefox-89.0.2::gentoo
 * Updating .desktop files database ...
 [ ok ]
 * Updating icons cache ...
 [ ok ]
 * Updating .desktop files database ...
 [ ok ]
 * Updating icons cache ...
 [ ok ]
 * USE='-gmp-autoupdate' has disabled the following plugins from updating or
 * installing into new profiles:
 *       gmp-gmpopenh264
 *       gmp-widevinecdm
 *

>>> Emerging binary (134 of 187) sys-auth/pambase-20210201.1::gentoo
 * pambase-20210201.1.tbz2 size ;-) ...                                  [ ok ]
>>> Extracting info
>>> Extracting sys-auth/pambase-20210201.1

>>> Installing (134 of 187) sys-auth/pambase-20210201.1::gentoo

>>> Emerging binary (135 of 187) net-misc/openssh-8.6_p1-r2::gentoo
 * openssh-8.6_p1-r2.tbz2 size ;-) ...                                   [ ok ]
>>> Extracting info
>>> Extracting net-misc/openssh-8.6_p1-r2

>>> Installing (135 of 187) net-misc/openssh-8.6_p1-r2::gentoo
 * >>> SetUID: [chmod go-r] /usr/lib64/misc/ssh-keysign ...
 [ ok ]

>>> Emerging binary (136 of 187) sys-apps/accountsservice-0.6.55-r1::gentoo
 * accountsservice-0.6.55-r1.tbz2 size ;-) ...                           [ ok ]
>>> Extracting info
>>> Extracting sys-apps/accountsservice-0.6.55-r1

>>> Installing (136 of 187) sys-apps/accountsservice-0.6.55-r1::gentoo

>>> Emerging binary (137 of 187) net-misc/networkmanager-1.30.4::gentoo
 * networkmanager-1.30.4.tbz2 size ;-) ...                               [ ok ]
>>> Extracting info
 * Checking for suitable kernel configuration options...
 [ ok ]
>>> Extracting net-misc/networkmanager-1.30.4

>>> Installing (137 of 187) net-misc/networkmanager-1.30.4::gentoo
 * You have enabled USE=dhclient and/or USE=dhcpcd, but NetworkManager since
 * version 1.20 defaults to the internal DHCP client. If the internal client
 * works for you, and you're happy with, the alternative USE flags can be
 * disabled. If you want to use dhclient or dhcpcd, then you need to tweak
 * the main.dhcp configuration option to use one of them instead of internal.

>>> Emerging binary (138 of 187) net-mail/mailutils-3.12-r2::gentoo
 * mailutils-3.12-r2.tbz2 size ;-) ...                                   [ ok ]
>>> Extracting info
>>> Extracting net-mail/mailutils-3.12-r2

>>> Installing (138 of 187) net-mail/mailutils-3.12-r2::gentoo
 * >>> SetGID: [chmod o-r] /usr/bin/dotlock ...
 [ ok ]

>>> Emerging binary (139 of 187) dev-qt/qtnetwork-5.15.2-r2::gentoo
 * qtnetwork-5.15.2-r2.tbz2 size ;-) ...                                 [ ok ]
>>> Extracting info
>>> Extracting dev-qt/qtnetwork-5.15.2-r2

>>> Installing (139 of 187) dev-qt/qtnetwork-5.15.2-r2::gentoo
 * Regenerating gentoo-qconfig.h
 * Updating QT_CONFIG in qconfig.pri
 * Updating QT.global_private in qmodule.pri

>>> Emerging binary (140 of 187) kde-plasma/kdecoration-5.22.2::gentoo
 * kdecoration-5.22.2.tbz2 size ;-) ...                                  [ ok ]
>>> Extracting info
>>> Extracting kde-plasma/kdecoration-5.22.2

>>> Installing (140 of 187) kde-plasma/kdecoration-5.22.2::gentoo

>>> Emerging binary (141 of 187) kde-plasma/layer-shell-qt-5.22.2::gentoo
 * layer-shell-qt-5.22.2.tbz2 size ;-) ...                               [ ok ]
>>> Extracting info
>>> Extracting kde-plasma/layer-shell-qt-5.22.2

>>> Installing (141 of 187) kde-plasma/layer-shell-qt-5.22.2::gentoo

>>> Emerging binary (142 of 187) dev-python/PyQt5-5.15.4-r1::gentoo
 * PyQt5-5.15.4-r1.tbz2 size ;-) ...                                     [ ok ]
>>> Extracting info
>>> Extracting dev-python/PyQt5-5.15.4-r1

>>> Installing (142 of 187) dev-python/PyQt5-5.15.4-r1::gentoo

>>> Emerging binary (143 of 187) kde-plasma/libkscreen-5.22.2::gentoo
 * libkscreen-5.22.2.tbz2 size ;-) ...                                   [ ok ]
>>> Extracting info
>>> Extracting kde-plasma/libkscreen-5.22.2

>>> Installing (143 of 187) kde-plasma/libkscreen-5.22.2::gentoo

>>> Emerging binary (144 of 187) kde-plasma/kwayland-server-5.22.2::gentoo
 * kwayland-server-5.22.2.tbz2 size ;-) ...                              [ ok ]
>>> Extracting info
>>> Extracting kde-plasma/kwayland-server-5.22.2

>>> Installing (144 of 187) kde-plasma/kwayland-server-5.22.2::gentoo

>>> Emerging binary (145 of 187) kde-plasma/kwayland-integration-5.22.2::gentoo
 * kwayland-integration-5.22.2.tbz2 size ;-) ...                         [ ok ]
>>> Extracting info
>>> Extracting kde-plasma/kwayland-integration-5.22.2

>>> Installing (145 of 187) kde-plasma/kwayland-integration-5.22.2::gentoo

>>> Emerging binary (146 of 187) kde-plasma/kwrited-5.22.2::gentoo
 * kwrited-5.22.2.tbz2 size ;-) ...                                      [ ok ]
>>> Extracting info
>>> Extracting kde-plasma/kwrited-5.22.2

>>> Installing (146 of 187) kde-plasma/kwrited-5.22.2::gentoo

>>> Emerging binary (147 of 187) media-video/vidcutter-6.0.5::gentoo
 * vidcutter-6.0.5.tbz2 size ;-) ...                                     [ ok ]
>>> Extracting info
>>> Extracting media-video/vidcutter-6.0.5

>>> Installing (147 of 187) media-video/vidcutter-6.0.5::gentoo
 * Updating .desktop files database ...
 [ ok ]
 * Updating icons cache ...
 [ ok ]
 * Updating shared mime info database ...
 [ ok ]
 * Updating .desktop files database ...
 [ ok ]
 * Updating icons cache ...
 [ ok ]
 * Updating shared mime info database ...
 [ ok ]

>>> Emerging binary (148 of 187) kde-plasma/kgamma-5.22.2::gentoo
 * kgamma-5.22.2.tbz2 size ;-) ...                                       [ ok ]
>>> Extracting info
>>> Extracting kde-plasma/kgamma-5.22.2

>>> Installing (148 of 187) kde-plasma/kgamma-5.22.2::gentoo

>>> Emerging binary (149 of 187) kde-plasma/polkit-kde-agent-5.22.2::gentoo
 * polkit-kde-agent-5.22.2.tbz2 size ;-) ...                             [ ok ]
>>> Extracting info
>>> Extracting kde-plasma/polkit-kde-agent-5.22.2

>>> Installing (149 of 187) kde-plasma/polkit-kde-agent-5.22.2::gentoo
 * Updating .desktop files database ...
 [ ok ]
 * Updating .desktop files database ...
 [ ok ]

>>> Emerging binary (150 of 187) kde-plasma/ksshaskpass-5.22.2::gentoo
 * ksshaskpass-5.22.2.tbz2 size ;-) ...                                  [ ok ]
>>> Extracting info
>>> Extracting kde-plasma/ksshaskpass-5.22.2

>>> Installing (150 of 187) kde-plasma/ksshaskpass-5.22.2::gentoo
 * In order to have ssh-agent start with Plasma 5,
 * edit /etc/xdg/plasma-workspace/env/10-agent-startup.sh
 * and uncomment the lines enabling ssh-agent.
 *
 * If you do so, do not forget to uncomment the respective
 * lines in /etc/xdg/plasma-workspace/shutdown/10-agent-shutdown.sh
 * to properly kill the agent when the session ends.
 *
 * ksshaskpass has been installed as your default askpass application
 * for Plasma 5 sessions.
 * If that's not desired, select the one you want to use in
 * /etc/xdg/plasma-workspace/env/05-ksshaskpass.sh
rmdir: removing directory, '/etc/plasma/startup'
rmdir: removing directory, '/etc/plasma'

>>> Emerging binary (151 of 187) kde-plasma/kwallet-pam-5.22.2::gentoo
 * kwallet-pam-5.22.2.tbz2 size ;-) ...                                  [ ok ]
>>> Extracting info
>>> Extracting kde-plasma/kwallet-pam-5.22.2

>>> Installing (151 of 187) kde-plasma/kwallet-pam-5.22.2::gentoo
 * This package enables auto-unlocking of kde-frameworks/kwallet:5.
 * See also: https://wiki.gentoo.org/wiki/KDE#KWallet_auto-unlocking

>>> Emerging binary (152 of 187) kde-plasma/kactivitymanagerd-5.22.2::gentoo
 * kactivitymanagerd-5.22.2.tbz2 size ;-) ...                            [ ok ]
>>> Extracting info
>>> Extracting kde-plasma/kactivitymanagerd-5.22.2

>>> Installing (152 of 187) kde-plasma/kactivitymanagerd-5.22.2::gentoo

>>> Emerging binary (153 of 187) kde-plasma/kmenuedit-5.22.2::gentoo
 * kmenuedit-5.22.2.tbz2 size ;-) ...                                    [ ok ]
>>> Extracting info
>>> Extracting kde-plasma/kmenuedit-5.22.2

>>> Installing (153 of 187) kde-plasma/kmenuedit-5.22.2::gentoo
 * Updating .desktop files database ...
 [ ok ]
 * Updating icons cache ...
 [ ok ]
 * Updating .desktop files database ...
 [ ok ]
 * Updating icons cache ...
 [ ok ]

>>> Emerging binary (154 of 187) kde-plasma/drkonqi-5.22.2::gentoo
 * drkonqi-5.22.2.tbz2 size ;-) ...                                      [ ok ]
>>> Extracting info
>>> Extracting kde-plasma/drkonqi-5.22.2

>>> Installing (154 of 187) kde-plasma/drkonqi-5.22.2::gentoo
 * Updating .desktop files database ...
 [ ok ]
 * Updating .desktop files database ...
 [ ok ]

>>> Emerging binary (155 of 187) app-office/libreoffice-7.1.4.2::gentoo
 * libreoffice-7.1.4.2.tbz2 size ;-) ...                                 [ ok ]
>>> Extracting info
>>> Extracting app-office/libreoffice-7.1.4.2

>>> Installing (155 of 187) app-office/libreoffice-7.1.4.2::gentoo
 * Updating icons cache ...
 [ ok ]
 * Updating .desktop files database ...
 [ ok ]
 * Updating shared mime info database ...
 [ ok ]
 * Updating icons cache ...
 [ ok ]
 * Updating .desktop files database ...
 [ ok ]
 * Updating shared mime info database ...
 [ ok ]

>>> Emerging binary (156 of 187) kde-plasma/libksysguard-5.22.2::gentoo
 * libksysguard-5.22.2.tbz2 size ;-) ...                                 [ ok ]
>>> Extracting info
>>> Extracting kde-plasma/libksysguard-5.22.2

>>> Installing (156 of 187) kde-plasma/libksysguard-5.22.2::gentoo

>>> Emerging binary (157 of 187) kde-plasma/ksystemstats-5.22.2::gentoo
 * ksystemstats-5.22.2.tbz2 size ;-) ...                                 [ ok ]
>>> Extracting info
>>> Extracting kde-plasma/ksystemstats-5.22.2

>>> Installing (157 of 187) kde-plasma/ksystemstats-5.22.2::gentoo

>>> Emerging binary (158 of 187) kde-plasma/plasma-thunderbolt-5.22.2::gentoo
 * plasma-thunderbolt-5.22.2.tbz2 size ;-) ...                           [ ok ]
>>> Extracting info
>>> Extracting kde-plasma/plasma-thunderbolt-5.22.2

>>> Installing (158 of 187) kde-plasma/plasma-thunderbolt-5.22.2::gentoo

>>> Emerging binary (159 of 187) kde-plasma/xdg-desktop-portal-kde-5.22.2::gentoo
 * xdg-desktop-portal-kde-5.22.2.tbz2 size ;-) ...                       [ ok ]
>>> Extracting info
>>> Extracting kde-plasma/xdg-desktop-portal-kde-5.22.2

>>> Installing (159 of 187) kde-plasma/xdg-desktop-portal-kde-5.22.2::gentoo
 * Updating .desktop files database ...
 [ ok ]
 * Updating .desktop files database ...
 [ ok ]

>>> Emerging binary (160 of 187) kde-plasma/plasma-systemmonitor-5.22.2::gentoo
 * plasma-systemmonitor-5.22.2.tbz2 size ;-) ...                         [ ok ]
>>> Extracting info
>>> Extracting kde-plasma/plasma-systemmonitor-5.22.2

>>> Installing (160 of 187) kde-plasma/plasma-systemmonitor-5.22.2::gentoo
 * Updating .desktop files database ...
 [ ok ]
 * Updating .desktop files database ...
 [ ok ]

>>> Emerging binary (161 of 187) kde-plasma/plasma-vault-5.22.2::gentoo
 * plasma-vault-5.22.2.tbz2 size ;-) ...                                 [ ok ]
>>> Extracting info
>>> Extracting kde-plasma/plasma-vault-5.22.2

>>> Installing (161 of 187) kde-plasma/plasma-vault-5.22.2::gentoo

>>> Emerging binary (162 of 187) kde-plasma/plasma-pa-5.22.2::gentoo
 * plasma-pa-5.22.2.tbz2 size ;-) ...                                    [ ok ]
>>> Extracting info
>>> Extracting kde-plasma/plasma-pa-5.22.2

>>> Installing (162 of 187) kde-plasma/plasma-pa-5.22.2::gentoo

>>> Emerging binary (163 of 187) kde-plasma/milou-5.22.2::gentoo
 * milou-5.22.2.tbz2 size ;-) ...                                        [ ok ]
>>> Extracting info
>>> Extracting kde-plasma/milou-5.22.2

>>> Installing (163 of 187) kde-plasma/milou-5.22.2::gentoo

>>> Emerging binary (164 of 187) sys-apps/flatpak-1.11.1::gentoo
 * flatpak-1.11.1.tbz2 size ;-) ...                                      [ ok ]
>>> Extracting info
 * Checking for suitable kernel configuration options...
 [ ok ]
>>> Extracting sys-apps/flatpak-1.11.1

>>> Installing (164 of 187) sys-apps/flatpak-1.11.1::gentoo

>>> Emerging binary (165 of 187) kde-plasma/discover-5.22.2::gentoo
 * discover-5.22.2.tbz2 size ;-) ...                                     [ ok ]
>>> Extracting info
>>> Extracting kde-plasma/discover-5.22.2

>>> Installing (165 of 187) kde-plasma/discover-5.22.2::gentoo
 * Updating .desktop files database ...
 [ ok ]
 * Updating icons cache ...
 [ ok ]
 * Updating .desktop files database ...
 [ ok ]
 * Updating icons cache ...
 [ ok ]

>>> Emerging binary (166 of 187) kde-plasma/breeze-5.22.2::gentoo
 * breeze-5.22.2.tbz2 size ;-) ...                                       [ ok ]
>>> Extracting info
>>> Extracting kde-plasma/breeze-5.22.2

>>> Installing (166 of 187) kde-plasma/breeze-5.22.2::gentoo
 * Updating icons cache ...
 [ ok ]
 * Updating icons cache ...
 [ ok ]

>>> Emerging binary (167 of 187) kde-plasma/kscreenlocker-5.22.2::gentoo
 * kscreenlocker-5.22.2.tbz2 size ;-) ...                                [ ok ]
>>> Extracting info
>>> Extracting kde-plasma/kscreenlocker-5.22.2

>>> Installing (167 of 187) kde-plasma/kscreenlocker-5.22.2::gentoo

>>> Emerging binary (168 of 187) kde-plasma/libkworkspace-5.22.2::gentoo
 * libkworkspace-5.22.2.tbz2 size ;-) ...                                [ ok ]
>>> Extracting info
>>> Extracting kde-plasma/libkworkspace-5.22.2

>>> Installing (168 of 187) kde-plasma/libkworkspace-5.22.2::gentoo

>>> Emerging binary (169 of 187) kde-plasma/kde-cli-tools-5.22.2::gentoo
 * kde-cli-tools-5.22.2.tbz2 size ;-) ...                                [ ok ]
>>> Extracting info
>>> Extracting kde-plasma/kde-cli-tools-5.22.2

>>> Installing (169 of 187) kde-plasma/kde-cli-tools-5.22.2::gentoo
 * Updating .desktop files database ...
 [ ok ]
 * Updating .desktop files database ...
 [ ok ]

>>> Emerging binary (170 of 187) kde-plasma/kwin-5.22.2::gentoo
 * kwin-5.22.2.tbz2 size ;-) ...                                         [ ok ]
>>> Extracting info
>>> Extracting kde-plasma/kwin-5.22.2

>>> Installing (170 of 187) kde-plasma/kwin-5.22.2::gentoo
 * Updating icons cache ...
 [ ok ]
 * Updating icons cache ...
 [ ok ]
 * Install additional packages for optional runtime features:
 *   x11-misc/colord for color management support
 *
 * In Plasma 5.20, default behavior of the Task Switcher to move minimised
 * windows to the end of the list was changed so that it remains in the
 * original order. To revert to the well established behavior:
 *
 *  - Edit ~/.config/kwinrc
 *  - Find [TabBox] section
 *  - Add "MoveMinimizedWindowsToEndOfTabBoxFocusChain=true"

>>> Emerging binary (171 of 187) kde-plasma/oxygen-5.22.2::gentoo
 * oxygen-5.22.2.tbz2 size ;-) ...                                       [ ok ]
>>> Extracting info
>>> Extracting kde-plasma/oxygen-5.22.2

>>> Installing (171 of 187) kde-plasma/oxygen-5.22.2::gentoo
 * Updating icons cache ...
 [ ok ]
 * Updating icons cache ...
 [ ok ]

>>> Emerging binary (172 of 187) kde-plasma/plasma-integration-5.22.2::gentoo
 * plasma-integration-5.22.2.tbz2 size ;-) ...                           [ ok ]
>>> Extracting info
>>> Extracting kde-plasma/plasma-integration-5.22.2

>>> Installing (172 of 187) kde-plasma/plasma-integration-5.22.2::gentoo

>>> Emerging binary (173 of 187) kde-plasma/systemsettings-5.22.2::gentoo
 * systemsettings-5.22.2.tbz2 size ;-) ...                               [ ok ]
>>> Extracting info
>>> Extracting kde-plasma/systemsettings-5.22.2

>>> Installing (173 of 187) kde-plasma/systemsettings-5.22.2::gentoo
 * Updating .desktop files database ...
 [ ok ]
 * Updating .desktop files database ...
 [ ok ]

>>> Emerging binary (174 of 187) kde-plasma/khotkeys-5.22.2::gentoo
 * khotkeys-5.22.2.tbz2 size ;-) ...                                     [ ok ]
>>> Extracting info
>>> Extracting kde-plasma/khotkeys-5.22.2

>>> Installing (174 of 187) kde-plasma/khotkeys-5.22.2::gentoo

>>> Emerging binary (175 of 187) kde-plasma/kscreen-5.22.2::gentoo
 * kscreen-5.22.2.tbz2 size ;-) ...                                      [ ok ]
>>> Extracting info
>>> Extracting kde-plasma/kscreen-5.22.2

>>> Installing (175 of 187) kde-plasma/kscreen-5.22.2::gentoo

>>> Emerging binary (176 of 187) kde-plasma/powerdevil-5.22.2::gentoo
 * powerdevil-5.22.2.tbz2 size ;-) ...                                   [ ok ]
>>> Extracting info
>>> Extracting kde-plasma/powerdevil-5.22.2

>>> Installing (176 of 187) kde-plasma/powerdevil-5.22.2::gentoo

>>> Emerging binary (177 of 187) kde-plasma/bluedevil-5.22.2::gentoo
 * bluedevil-5.22.2.tbz2 size ;-) ...                                    [ ok ]
>>> Extracting info
>>> Extracting kde-plasma/bluedevil-5.22.2

>>> Installing (177 of 187) kde-plasma/bluedevil-5.22.2::gentoo
 * Updating .desktop files database ...
 [ ok ]
 * Updating shared mime info database ...
 [ ok ]
 * Updating .desktop files database ...
 [ ok ]
 * Updating shared mime info database ...
 [ ok ]

>>> Emerging binary (178 of 187) kde-plasma/sddm-kcm-5.22.2::gentoo
 * sddm-kcm-5.22.2.tbz2 size ;-) ...                                     [ ok ]
>>> Extracting info
>>> Extracting kde-plasma/sddm-kcm-5.22.2

>>> Installing (178 of 187) kde-plasma/sddm-kcm-5.22.2::gentoo

>>> Emerging binary (179 of 187) kde-plasma/kde-gtk-config-5.22.2::gentoo
 * kde-gtk-config-5.22.2.tbz2 size ;-) ...                               [ ok ]
>>> Extracting info
>>> Extracting kde-plasma/kde-gtk-config-5.22.2

>>> Installing (179 of 187) kde-plasma/kde-gtk-config-5.22.2::gentoo
 * If you notice missing icons in your GTK applications, you may have to install
 * the corresponding themes for GTK. A good guess would be x11-themes/oxygen-gtk
 * for example.

>>> Emerging binary (180 of 187) kde-plasma/plasma-nm-5.22.2::gentoo
 * plasma-nm-5.22.2.tbz2 size ;-) ...                                    [ ok ]
>>> Extracting info
>>> Extracting kde-plasma/plasma-nm-5.22.2

>>> Installing (180 of 187) kde-plasma/plasma-nm-5.22.2::gentoo

>>> Emerging binary (181 of 187) kde-plasma/kinfocenter-5.22.2-r10::redcore
 * kinfocenter-5.22.2-r10.tbz2 size ;-) ...                              [ ok ]
>>> Extracting info
>>> Extracting kde-plasma/kinfocenter-5.22.2-r10

>>> Installing (181 of 187) kde-plasma/kinfocenter-5.22.2-r10::redcore
 * Updating .desktop files database ...
 [ ok ]
 * Updating .desktop files database ...
 [ ok ]

>>> Emerging binary (182 of 187) kde-plasma/plasma-disks-5.22.2::gentoo
 * plasma-disks-5.22.2.tbz2 size ;-) ...                                 [ ok ]
>>> Extracting info
>>> Extracting kde-plasma/plasma-disks-5.22.2

>>> Installing (182 of 187) kde-plasma/plasma-disks-5.22.2::gentoo

>>> Emerging binary (183 of 187) kde-plasma/plasma-workspace-5.22.2::gentoo
 * plasma-workspace-5.22.2.tbz2 size ;-) ...                             [ ok ]
>>> Extracting info
>>> Extracting kde-plasma/plasma-workspace-5.22.2

>>> Installing (183 of 187) kde-plasma/plasma-workspace-5.22.2::gentoo
 * Updating .desktop files database ...
 [ ok ]
 * Updating icons cache ...
 [ ok ]
 * Updating .desktop files database ...
 [ ok ]
 * Updating icons cache ...
 [ ok ]
 * To enable gpg-agent and/or ssh-agent in Plasma sessions,
 * edit /etc/xdg/plasma-workspace/env/10-agent-startup.sh
 * and /etc/xdg/plasma-workspace/shutdown/10-agent-shutdown.sh

>>> Emerging binary (184 of 187) kde-plasma/kdeplasma-addons-5.22.2::gentoo
 * kdeplasma-addons-5.22.2.tbz2 size ;-) ...                             [ ok ]
>>> Extracting info
>>> Extracting kde-plasma/kdeplasma-addons-5.22.2

>>> Installing (184 of 187) kde-plasma/kdeplasma-addons-5.22.2::gentoo
 * Updating icons cache ...
 [ ok ]
 * Updating icons cache ...
 [ ok ]

>>> Emerging binary (185 of 187) kde-plasma/plasma-desktop-5.22.2::gentoo
 * plasma-desktop-5.22.2.tbz2 size ;-) ...                               [ ok ]
>>> Extracting info
>>> Extracting kde-plasma/plasma-desktop-5.22.2

>>> Installing (185 of 187) kde-plasma/plasma-desktop-5.22.2::gentoo
 * Updating .desktop files database ...
 [ ok ]
 * Updating icons cache ...
 [ ok ]
 * Updating .desktop files database ...
 [ ok ]
 * Updating icons cache ...
 [ ok ]

>>> Emerging binary (186 of 187) kde-plasma/plasma-browser-integration-5.22.2::gentoo
 * plasma-browser-integration-5.22.2.tbz2 size ;-) ...                   [ ok ]
>>> Extracting info
>>> Extracting kde-plasma/plasma-browser-integration-5.22.2

>>> Installing (186 of 187) kde-plasma/plasma-browser-integration-5.22.2::gentoo
 * Updating .desktop files database ...
 [ ok ]
 * Updating .desktop files database ...
 [ ok ]

>>> Emerging binary (187 of 187) kde-plasma/plasma-meta-5.22.2::gentoo
 * plasma-meta-5.22.2.tbz2 size ;-) ...                                  [ ok ]
>>> Extracting info
>>> Extracting kde-plasma/plasma-meta-5.22.2

>>> Installing (187 of 187) kde-plasma/plasma-meta-5.22.2::gentoo
 * Messages for package dev-db/firebird-3.0.4.33054.0:
 * Adding group 'firebird' to your system ...
 *  - Groupid: 450
 * Adding user 'firebird' to your system ...
 *  - Userid: 450
 *  - Shell: /bin/sh
 *  - Home: /usr/lib64/firebird
 *  - Groups: firebird
 *  - GECOS: added by portage for firebird
 *  - Creating /usr/lib64/firebird in
 * Messages for package dev-lang/ruby-2.5.9-r1:
 *
 * To switch between available Ruby profiles, execute as root:
 *      eselect ruby set ruby(23|24|...)
 *
 * Messages for package sys-apps/util-linux-2.37:
 * The mesg/wall/write tools have been disabled due to USE=-tty-helpers.
 * Messages for package dev-libs/boost-1.76.0-r1:
 * Boost.Regex is *extremely* ABI sensitive. If you get errors such as
 *
 *   undefined reference to `boost::re_detail_107600::cpp_regex_traits_implementation
 *     <char>::transform_primary[abi:cxx11](char const*, char const*) const'
 *
 * Then you need to recompile Boost and all its reverse dependencies
 * using the same toolchain. In general, *every* change of the C++ toolchain
 * requires a complete rebuild of the Boost-dependent ecosystem.
 *
 * See for instance https://bugs.gentoo.org/638138
 * Messages for package sys-process/cronie-1.5.7:
 * You should restart cronie daemon or else you might experience segfaults
 * or cronie not working reliably anymore.
 * Messages for package dev-python/mako-1.1.4:
 * Install additional packages for optional runtime features:
 *   dev-python/beaker for caching support
 * Messages for package app-arch/brotli-1.0.9-r1:
 *
 * Installation of a directory is blocked by a file:
 *   '/usr/lib/python3.9/site-packages/Brotli-1.0.9-py3.9.egg-info'
 * This file will be renamed to a different name:
 *   '/usr/lib/python3.9/site-packages/Brotli-1.0.9-py3.9.egg-info.backup.0000'
 *
 * Messages for package dev-python/jinja-3.0.1:
 * For i18n support, please emerge dev-python/Babel.
 * Messages for package dev-python/lxml-4.6.3-r1:
 * Install additional packages for optional runtime features:
 *   dev-python/beautifulsoup for Support for BeautifulSoup as a parser backend
 *   dev-python/cssselect for Translates CSS selectors to XPath 1.0 expressions
 * Messages for package dev-lang/rust-1.53.0:
 * Rust installs a helper script for calling GDB and LLDB,
 * for your convenience it is installed under /usr/bin/rust-{gdb,lldb}-1.53.0.
 * install app-vim/rust-vim to get vim support for rust.
 * Messages for package net-dialup/ppp-2.4.9-r3:
 * Pon, poff and plog scripts have been supplied for experienced users.
 * Users needing particular scripts (ssh,rsh,etc.) should check out the
 * /usr/share/doc/ppp-2.4.9-r3/scripts directory.
 * "rp-pppoe.so" plugin has been renamed to "pppoe.so"
 * Messages for package dev-java/openjdk-bin-11.0.11_p9-r1:
 * The experimental gentoo-vm USE flag has not been enabled so this JDK
 * will not be recognised by the system. For example, simply calling
 * "java" will launch a different JVM. This is necessary until Gentoo
 * fully supports Java 11. This JDK must therefore be invoked using its
 * absolute location under /opt/openjdk-bin-11.0.11_p9.
 * Messages for package dev-java/openjdk-jre-bin-11.0.11_p9:
 * The experimental gentoo-vm USE flag has not been enabled so this JRE
 * will not be recognised by the system. For example, simply calling
 * "java" will launch a different JVM. This is necessary until Gentoo
 * fully supports Java 11. This JRE must therefore be invoked using its
 * absolute location under /opt/openjdk-jre-bin-11.0.11_p9.
 * Messages for package media-video/mpv-0.33.1-r1:
 * Install additional packages for optional runtime features:
 *   net-misc/youtube-dl for URL support
 * Messages for package www-client/firefox-89.0.2:
 * USE='-gmp-autoupdate' has disabled the following plugins from updating or
 * installing into new profiles:
 *       gmp-gmpopenh264
 *       gmp-widevinecdm
 *
 * Messages for package net-misc/networkmanager-1.30.4:
 * You have enabled USE=dhclient and/or USE=dhcpcd, but NetworkManager since
 * version 1.20 defaults to the internal DHCP client. If the internal client
 * works for you, and you're happy with, the alternative USE flags can be
 * disabled. If you want to use dhclient or dhcpcd, then you need to tweak
 * the main.dhcp configuration option to use one of them instead of internal.
 * Messages for package kde-plasma/ksshaskpass-5.22.2:
 * In order to have ssh-agent start with Plasma 5,
 * edit /etc/xdg/plasma-workspace/env/10-agent-startup.sh
 * and uncomment the lines enabling ssh-agent.
 *
 * If you do so, do not forget to uncomment the respective
 * lines in /etc/xdg/plasma-workspace/shutdown/10-agent-shutdown.sh
 * to properly kill the agent when the session ends.
 *
 * ksshaskpass has been installed as your default askpass application
 * for Plasma 5 sessions.
 * If that's not desired, select the one you want to use in
 * /etc/xdg/plasma-workspace/env/05-ksshaskpass.sh
 * Messages for package kde-plasma/kwallet-pam-5.22.2:
 * This package enables auto-unlocking of kde-frameworks/kwallet:5.
 * See also: https://wiki.gentoo.org/wiki/KDE#KWallet_auto-unlocking
 * Messages for package kde-plasma/kwin-5.22.2:
 * Install additional packages for optional runtime features:
 *   x11-misc/colord for color management support
 *
 * In Plasma 5.20, default behavior of the Task Switcher to move minimised
 * windows to the end of the list was changed so that it remains in the
 * original order. To revert to the well established behavior:
 *
 *  - Edit ~/.config/kwinrc
 *  - Find [TabBox] section
 *  - Add "MoveMinimizedWindowsToEndOfTabBoxFocusChain=true"
 * Messages for package kde-plasma/kde-gtk-config-5.22.2:
 * If you notice missing icons in your GTK applications, you may have to install
 * the corresponding themes for GTK. A good guess would be x11-themes/oxygen-gtk
 * for example.
 * Messages for package kde-plasma/plasma-workspace-5.22.2:
 * To enable gpg-agent and/or ssh-agent in Plasma sessions,
 * edit /etc/xdg/plasma-workspace/env/10-agent-startup.sh
 * and /etc/xdg/plasma-workspace/shutdown/10-agent-shutdown.sh










































>>> Auto-cleaning packages...

>>> No outdated packages were found on your system.

 * Regenerating GNU info directory index...
 * Processed 122 info files.

 * IMPORTANT: 2 config files in '/etc' need updating.
 * See the CONFIGURATION FILES and CONFIGURATION FILES UPDATE TOOLS
 * sections of the emerge man page to learn how to update config files.
 * After world updates, it is important to remove obsolete packages with
 * emerge --depclean. Refer to `man emerge` for more information.

 * IMPORTANT: 1 news items need reading for repository 'gentoo'.
 * Use eselect news read to view new items.


real    43m23.393s
user    31m5.120s
sys     7m41.553s
RedCoreRRTVB /home/user1 # 


