To switch to unstable channel:
nix-channel --add https://nixos.org/channels/nixos-unstable nixos


Standard update command:

nixos-rebuild switch --upgrade

